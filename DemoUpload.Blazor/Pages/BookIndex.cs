﻿using DemoUpload.Blazor.Models;
using DemoUpload.Blazor.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoUpload.Blazor.Pages
{
    public partial class BookIndex
    {
        public IEnumerable<BookModel> Model { get; set; }

        public BookFormModel Form { get; set; }

        [Inject]
        public BookService Service { get; set; }

        [Inject]
        public IJSRuntime jsRuntime { get; set; }


        public BookIndex()
        {
            Form = new BookFormModel();
        }

        protected async override Task OnInitializedAsync()
        {
            Model = await Service.GetAsync();
        }


        public async Task AddAsync()
        {
            await Service.InsertAsync(Form);
            Model = await Service.GetAsync();
            StateHasChanged();
        }

        async void OnFileChanged()
        {
            string blob = await jsRuntime.InvokeAsync<string>("GetBlob");
            Form.ImageFile = Convert.FromBase64String(blob.Split(",")[1]);
            Form.ImageMimeType = blob.Split(",")[0].Replace("data:", "").Replace(";base64", "");
        }
    }
}
