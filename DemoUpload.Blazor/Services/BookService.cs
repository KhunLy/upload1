﻿using DemoUpload.Blazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace DemoUpload.Blazor.Services
{
    public class BookService
    {
        private HttpClient client;

        public BookService(HttpClient client)
        {
            this.client = client;
        }

        public async Task<IEnumerable<BookModel>> GetAsync()
        {
            return await client.GetFromJsonAsync<IEnumerable<BookModel>>("book");
        }

        public async Task InsertAsync(BookFormModel form)
        {
            await client.PostAsJsonAsync("book", form);
        }
    }
}
